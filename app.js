document.addEventListener('DOMContentLoaded', () => {
    const WIDTH = 10
    const GRID = document.querySelector('.grid')
    const SCOREDISPLAY = document.querySelector('#score')
    const STATUS = document.querySelector('#status')
    const STARTBTN = document.querySelector('#start-button')
    let displaySquares = Array.from(document.querySelectorAll('.minigrid div'))
    let squares = Array.from(document.querySelectorAll('.grid div'))
    let displayIndex = 0
    let timerId
    let score = 0
    const color = [
        'red',
        'orange',
        'blue',
        'cyan',
        'brown',
        'green',
        'magenta'
    ]

    const ILTETROMINO = [
        [0, WIDTH, WIDTH * 2, 1],
        [0, 1, 2, WIDTH + 2],
        [1, WIDTH + 1, WIDTH * 2 + 1, WIDTH * 2],
        [0, WIDTH, WIDTH + 1, WIDTH + 2]
    ]

    const LTETROMINO = [
        [0, WIDTH, WIDTH * 2, WIDTH * 2 + 1],
        [0, 1, 2, WIDTH],
        [0, 1, WIDTH + 1, WIDTH * 2 + 1],
        [2, WIDTH, WIDTH + 1, WIDTH + 2],
    ]

    const ITETROMINO = [
        [0, WIDTH, WIDTH * 2, WIDTH * 3],
        [0, 1, 2, 3],
        [0, WIDTH, WIDTH * 2, WIDTH * 3],
        [0, 1, 2, 3]
    ]

    const OTETROMINO = [
        [0, WIDTH, WIDTH + 1, 1],
        [0, WIDTH, WIDTH + 1, 1],
        [0, WIDTH, WIDTH + 1, 1],
        [0, WIDTH, WIDTH + 1, 1]
    ]

    const STETROMINO = [
        [1, WIDTH, WIDTH + 1, 2],
        [0, WIDTH, WIDTH + 1, WIDTH * 2 + 1],
        [1, WIDTH, WIDTH + 1, 2],
        [0, WIDTH, WIDTH + 1, WIDTH * 2 + 1]
    ]

    const TTETROMINO = [
        [0, WIDTH, WIDTH * 2, WIDTH + 1],
        [0, 1, 2, WIDTH * 1 + 1],
        [1, WIDTH, WIDTH + 1, WIDTH * 2 + 1],
        [1, WIDTH, WIDTH + 1, WIDTH + 2]
    ]

    const ZTETROMINO = [
        [0, 1, WIDTH + 1, WIDTH + 2],
        [1, WIDTH + 1, WIDTH, WIDTH * 2],
        [0, 1, WIDTH + 1, WIDTH + 2],
        [1, WIDTH + 1, WIDTH, WIDTH * 2]
    ]

    const THETETROMINOES = [ILTETROMINO, LTETROMINO, ITETROMINO, OTETROMINO, STETROMINO, TTETROMINO, ZTETROMINO]
    let currentRotation = Math.floor(Math.random() * 4)
    let random = Math.floor(Math.random() * THETETROMINOES.length)
    let currentPos = 4
    let current = THETETROMINOES[random][currentRotation]
    currentRotation = Math.floor(Math.random() * 4)
    let nextRandom = Math.floor(Math.random() * THETETROMINOES.length)
    let next = THETETROMINOES[nextRandom][currentRotation]
    STATUS.innerHTML = 'PAUSED'

    function draw() {
        current.forEach(index => {
            squares[currentPos + index].classList.add('tetromino')
            squares[currentPos + index].style.backgroundColor = color[random]
        })
    }

    function undraw() {
        current.forEach(index => {
            squares[currentPos + index].classList.remove('tetromino')
            squares[currentPos + index].style.backgroundColor = ''
        })
    }

    function control(e) {
        if (e.keyCode === 37) {
            moveLeft()
        } else if (e.keyCode === 39) {
            moveRight()
        } else if (e.keyCode === 40) {
            moveDown()
        } else if (e.keyCode === 38) {
            rotate()
        }
    }
    document.addEventListener('keyup', control)

    function moveDown() {
        undraw()
        currentPos += WIDTH
        draw()
        freeze()
    }

    function freeze() {
        if (current.some(index => squares[currentPos + index + WIDTH].classList.contains('base'))) {
            current.forEach(index => squares[currentPos + index].classList.add('base'))
            current = next
            currentPos = 4
            random = nextRandom
            gameOver()
            draw()
            nextRandom = Math.floor(Math.random() * THETETROMINOES.length)
            currentRotation = Math.floor(Math.random() * 4)
            next = THETETROMINOES[nextRandom][currentRotation]
            displayShape()
            addScore()
        }

    }

    function moveLeft() {
        undraw()
        const isAtLeftEdge = current.some(index => (currentPos + index) % WIDTH === 0)
        if (!isAtLeftEdge)
            currentPos -= 1
        if (current.some(index => squares[currentPos + index].classList.contains('base'))) {
            currentPos += 1
        }
        draw()
    }

    function moveRight() {
        undraw()
        const isAtRightEdge = current.some(index => (currentPos + index + 1) % WIDTH === 0)
        if (!isAtRightEdge)
            currentPos += 1
        if (current.some(index => squares[currentPos + index].classList.contains('base'))) {
            currentPos -= 1
        }
        draw()
    }

    function rotate() {
        undraw()
        currentRotation++

        if (currentRotation >= current.length)
            currentRotation = 0

        current = THETETROMINOES[random][currentRotation]

        if ((current.some(index => (currentPos + index + 1) % WIDTH === 0) && current.some(index => (currentPos + index) % WIDTH === 0)) || (current.some(index => squares[currentPos + index + WIDTH].classList.contains('base')))) {
            currentRotation--
            if (currentRotation < 0)
                currentRotation = 3
            current = THETETROMINOES[random][currentRotation]
        }
        draw()
    }

    function displayShape() {
        displaySquares.forEach(square => {
            square.classList.remove('tetromino')
            square.style.backgroundColor = ''
        })
        next.forEach(index => {
            if (index > 29)
                index = index - 18
            else if (index > 19)
                index = index - 12
            else if (index > 9)
                index = index - 6
            displaySquares[displayIndex + index].classList.add('tetromino')
            displaySquares[displayIndex + index].style.backgroundColor = color[nextRandom]
        })

    }

    STARTBTN.addEventListener('click', () => {
        if (timerId) {
            clearInterval(timerId)
            STATUS.innerHTML = 'PAUSED'
            timerId = null
        } else {
            draw()
            STATUS.innerHTML = 'PLAYING'
            timerId = setInterval(moveDown, 1000)
            displayShape()
        }
    })

    function addScore() {
        for (let i = 0; i < 199; i += WIDTH) {
            const row = [i, i + 1, i + 2, i + 3, i + 4, i + 5, i + 6, i + 7, i + 8, i + 9]
            if (row.every(index => squares[index].classList.contains('base'))) {
                score += 10
                SCOREDISPLAY.innerHTML = ' ' + score
                row.forEach(index => {
                    squares[index].classList.remove('base')
                    squares[index].classList.remove('tetromino')
                    squares[index].style.backgroundColor = ''
                })
                const squaresRemoved = squares.splice(i, WIDTH)
                squares = squaresRemoved.concat(squares)
                squares.forEach(cell => GRID.appendChild(cell))
            }
        }
    }

    function gameOver() {
        if (current.some(index => squares[currentPos + index].classList.contains('base'))) {
            SCOREDISPLAY.innerHTML = ' ' + score
            STATUS.innerHTML = 'GAME OVER'
            clearInterval(timerId)
        }
    }

})
